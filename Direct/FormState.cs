﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hash
{
    public enum FormState
    {
        Add,
        Edit,
        Search, 
        Delete,
        Display
    }
}
