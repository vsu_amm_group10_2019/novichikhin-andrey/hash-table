﻿using System;
using System.Windows.Forms;

namespace Hash
{
    public partial class AddForm : Form
    {
        public Info Info { get; } = new Info();
        private FormState FormState;
        public AddForm(FormState formState, Info info = null)
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;//статическое масштабирование
            MaximizeBox = false;
            FormState = formState;
            switch (formState)
            {
                case FormState.Add:
                    {
                        Text = "Add car";                        
                        return;   
                    }                   
                case FormState.Edit:
                    {
                        textBox1.ReadOnly = true;
                        textBox1.Text = info.Number.ToString();
                        textBox2.Text = info.FIO;
                        textBox3.Text = info.Mark;
                        button1.Text = "Edit";
                        Text = "Edit car";
                        break;
                    }
                case FormState.Delete:
                    {
                        textBox2.ReadOnly = true;
                        textBox3.ReadOnly = true;
                        Text = "Delete info";
                        button1.Text = "Delete";
                        break;
                    }
                case FormState.Search:
                    {                        
                        button1.Text = FormState.ToString();
                        textBox2.ReadOnly = true;
                        textBox3.ReadOnly = true;
                        Text = "Search car";
                        break;
                    }
                case FormState.Display:
                    {
                     
                        textBox1.ReadOnly = true;
                        textBox1.Text = info.Number.ToString();
                        textBox2.Text = info.FIO;
                        textBox3.Text = info.Mark;
                        textBox2.ReadOnly = true;
                        textBox3.ReadOnly = true;
                        button1.Visible = false;
                        break;
                    }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {           
            try
            {
                if (FormState == FormState.Add && (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == ""))
                {
                        MessageBox.Show("Введите все поля");
                }
                else
                {
                    Info.Number = int.Parse(textBox1.Text);
                    if (FormState == FormState.Add || FormState == FormState.Edit)
                    {
                        button1.Text = "Add";
                        Info.FIO = textBox2.Text;
                        Info.Mark = textBox3.Text;
                    }
                    Close();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Поле с номером авто не заполнено. Повторите попытку");

                //info = null;
            }          
            
        }

        private void AddForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (FormState == FormState.Add && (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == ""))
            {
                MessageBox.Show("Введите все поля");
                e.Cancel = true;
            }
            else
            {
                Info.Number = int.Parse(textBox1.Text);
                Info.FIO = textBox2.Text;
                Info.Mark = textBox3.Text;  
            }
        }
    }
}
