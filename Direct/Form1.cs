﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace Hash
{
    public partial class Form1 : Form
    {
        private string FileName { get; set; }
        LineHashtable LineHash { get; set; } = new LineHashtable(100);
        public Form1()
        {
            InitializeComponent();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm addForm = new AddForm(FormState.Add);
            addForm.ShowDialog();
            
            LineHash.Add(addForm.Info);
            //if (addForm.Info.Number == 0)
              //  LineHash.Delete(0);
            Redraw();
        }

        private void Redraw()
        {
            List<Info> info = LineHash.GetData();
            dataGridView1.Rows.Clear();
            dataGridView1.RowCount = info.Count;
            for (int i = 0; i < info.Count; i++)
            {
                dataGridView1.Rows[i].Cells[0].Value = info[i].Number;
                dataGridView1.Rows[i].Cells[1].Value = info[i].FIO;
                dataGridView1.Rows[i].Cells[2].Value = info[i].Mark;
            }
        }

        private void editToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.Search);
            search.ShowDialog();
            long number = search.Info.Number;
            Info info = LineHash.Find(number);
            if (info != null)
            {
                AddForm edit = new AddForm(FormState.Edit, info);
                edit.ShowDialog();
                LineHash.Delete(number);
                LineHash.Add(edit.Info);
                Redraw();
            }
            else
            {
                MessageBox.Show("Human not founf");
            }
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.Search);
            search.ShowDialog();
            long number = search.Info.Number;
            Info info = LineHash.Find(number);
            if (info != null)
            {
               AddForm show = new AddForm(FormState.Display, info);
                show.ShowDialog(); 
            }
            else
            {
                MessageBox.Show("Human not found");
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm search = new AddForm(FormState.Delete);
            search.ShowDialog();
            long number = search.Info.Number;
            LineHash.Delete(number);
            Redraw();
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            openFileDialog1.Filter = "Json File|*.json";
            openFileDialog1.ShowDialog();
            if (!string.IsNullOrEmpty(openFileDialog1.FileName))
            {
                string txt = null;
                try
                {
                    FileName = openFileDialog1.FileName;
                    txt = File.ReadAllText(FileName);
                    List<Info> inform = JsonConvert.DeserializeObject<List<Info>>(txt);
                    foreach (Info info in inform)
                    {
                        LineHash.Add(info);
                    }
                    Redraw();
                }
                catch (System.IO.FileNotFoundException)
                {
                    MessageBox.Show("Ошибка!");
                    txt = null;
                }
               
            }
            
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {                
                SaveToFile();
            }
            else
            {
                saveAsToolStripMenuItem_Click(sender,e);
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Json File|*.json";
            saveFileDialog1.ShowDialog();
            if (!string.IsNullOrEmpty(saveFileDialog1.FileName))
            {
                FileName = saveFileDialog1.FileName;
                SaveToFile();
            }
        }

        private void SaveToFile ()
        {
            List<Info> info = LineHash.GetData();
            string resault = JsonConvert.SerializeObject(info);
            File.WriteAllText(FileName, resault);
        }

        private void MenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
